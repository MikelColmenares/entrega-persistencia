package es.deusto.asf.PracticaHibernate.dao;

import es.deusto.asf.PracticaHibernate.model.Faculty;
import es.deusto.asf.PracticaHibernate.model.Grade;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.hibernate.query.Query;
import org.springframework.orm.hibernate5.support.HibernateDaoSupport;

import javax.persistence.criteria.CriteriaQuery;
import java.io.Serializable;
import java.util.List;

public class GradeDAOImpl implements GradeDAO {
    private SessionFactory sessionFactory;

    public GradeDAOImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Grade> findGradesByFaculty(Faculty faculty) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        Query query = session.createQuery("from Grade where faculty.id = :faculty");
        query.setParameter("faculty", faculty.getId());
        transaction.commit();
        List<Grade> gradeList = query.list();
        session.close();

        return gradeList;
    }
}
