package es.deusto.asf.PracticaHibernate.dao;

import java.util.List;

import es.deusto.asf.PracticaHibernate.model.Faculty;
import es.deusto.asf.PracticaHibernate.model.Grade;

public interface GradeDAO {
	public List<Grade> findGradesByFaculty(Faculty faculty);
}
