package es.deusto.asf.PracticaHibernate.dao;

import es.deusto.asf.PracticaHibernate.model.Faculty;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import java.util.List;

public class FacultyDAOImpl implements FacultyDAO{
    private SessionFactory sessionFactory;

    public FacultyDAOImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Faculty> findAllFaculties() {
        Session session = sessionFactory.openSession();
        CriteriaQuery<Faculty> criteriaQuery = session.getCriteriaBuilder().createQuery(Faculty.class);
        criteriaQuery.from(Faculty.class);
        List<Faculty> Faculty = session.createQuery(criteriaQuery).getResultList();
        //session.close();

        return Faculty;
    }

    @Override
    public Faculty findFacultyById(String id) {
        Session session = sessionFactory.openSession();
        Faculty faculty =  (Faculty) session.get(Faculty.class,id);
        return faculty;
    }

}
