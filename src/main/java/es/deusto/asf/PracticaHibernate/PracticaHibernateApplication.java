package es.deusto.asf.PracticaHibernate;

import es.deusto.asf.PracticaHibernate.dao.FacultyDAO;
import es.deusto.asf.PracticaHibernate.dao.GradeDAO;
import es.deusto.asf.PracticaHibernate.model.Faculty;
import es.deusto.asf.PracticaHibernate.model.Grade;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.List;

@SpringBootApplication
public class PracticaHibernateApplication {

	public static void main(String[] args) {

		ApplicationContext applicationContext = new ClassPathXmlApplicationContext("applicationContext.xml");

		FacultyDAO facultyDAO = (FacultyDAO) applicationContext.getBean("facultyDAO");
        GradeDAO gradeDAO = (GradeDAO) applicationContext.getBean("gradeDAO");

        // Metodos creados
        List<Faculty> facultyList = null;
        facultyList = facultyDAO.findAllFaculties();
        Faculty faculty = facultyDAO.findFacultyById("F01");
        List<Grade> grade = gradeDAO.findGradesByFaculty(faculty);

		// Prueba del metodo findAllFaculties
		System.out.println("\n\n\n\n"+"Prueba del metodo findAllFaculties\n"+facultyList.get(0).getName());
        System.out.println(facultyList.get(1).getName());
        System.out.println(facultyList.get(2).getName()+"\n");


		// Prueba del metodo findFacultyById
		System.out.println("\n"+"Prueba del metodo findFacultyById"+"\nName = "+faculty.getName()+"\nurl = "+
                faculty.getUrl());

        // Prueba del metodo findGradesByFaculty
        System.out.println("\n"+"Prueba del metodo findGradesByFaculty");
        for (int i=0; i<grade.size();i++){
            System.out.println(grade.get(i).getName());
            System.out.println(grade.get(i).getFaculty()+"\n");
        }
	}

}
